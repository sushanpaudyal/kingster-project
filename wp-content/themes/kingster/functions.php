<?php
    // Includes Files
     include (get_template_directory() . '/front/enqueue.php');
     include (get_template_directory() . '/front/setup.php');
     include (get_template_directory() . '/front/theme-customizer.php');
     include (get_template_directory() . '/front/contact.php');
     include (get_template_directory() . '/front/social.php');
     include (get_template_directory() . '/front/banner.php');

    // Hooks
    add_action('wp_enqueue_scripts', 'kingster_styles');
    add_action('customize_register', 'kingster_customize_register');