<?php
   function kingster_contact_customizer_section($wp_customize){
       $wp_customize->add_setting('kingster_email_handle', array(
           'default' => ''
       ));
       $wp_customize->add_setting('kingster_phone_handle', array(
           'default' => ''
       ));
       $wp_customize->add_section('kingster_contact_section', [
           'title' => __('Contact Settings', 'kingster'),
           'priority' => 30,
           'panel' => 'basic'
       ]);
       $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'kingster_email_text_handle', array(
           'label' => __('Email Address', 'kingster'),
            'section' => 'kingster_contact_section',
           'settings' => 'kingster_email_handle',
           'description' => __('Please Enter Your Email Address', 'kingster'),
           'type' => 'text'
       )));
       $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'kingster_phone_text_handle', array(
           'label' => __('Phone Number', 'kingster'),
           'section' => 'kingster_contact_section',
           'settings' => 'kingster_phone_handle',
           'description' => __('Please Enter Your Phone Number', 'kingster'),
           'type' => 'text'
       )));
   }