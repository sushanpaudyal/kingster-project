<?php
function kingster_banner_customizer_section($wp_customize){
    $wp_customize->add_setting('kingster_title_1_handle', array(
        'default' => ''
    ));
    $wp_customize->add_setting('kingster_title_2_handle', array(
        'default' => ''
    ));
    $wp_customize->add_setting('kingster_title_3_handle', array(
        'default' => ''
    ));

    $wp_customize->add_setting('kingster_content_1_handle', array(
        'default' => ''
    ));
    $wp_customize->add_setting('kingster_content_2_handle', array(
        'default' => ''
    ));
    $wp_customize->add_setting('kingster_content_3_handle', array(
        'default' => ''
    ));

    $wp_customize->add_setting('kingster_image_1_handle', array(
        'transport' => 'refresh',
        'height' => 225
    ));

    $wp_customize->add_setting('kingster_image_2_handle', array(
        'transport' => 'refresh',
        'height' => 225
    ));

    $wp_customize->add_setting('kingster_image_3_handle', array(
        'transport' => 'refresh',
        'height' => 225
    ));

    $wp_customize->add_section('kingster_section_1_section', [
        'title' => __('Section 1', 'kingster'),
        'priority' => 30,
        'panel' => 'banner'
    ]);
    $wp_customize->add_section('kingster_section_2_section', [
        'title' => __('Section 2', 'kingster'),
        'priority' => 30,
        'panel' => 'banner'
    ]);
    $wp_customize->add_section('kingster_section_3_section', [
        'title' => __('Section 3', 'kingster'),
        'priority' => 30,
        'panel' => 'banner'
    ]);
    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'kingster_title_1_text_handle', array(
        'label' => __('Title', 'kingster'),
        'section' => 'kingster_section_1_section',
        'settings' => 'kingster_title_1_handle',
        'description' => __('Please Title', 'kingster'),
        'type' => 'text'
    )));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'kingster_title_2_text_handle', array(
        'label' => __('Title', 'kingster'),
        'section' => 'kingster_section_2_section',
        'settings' => 'kingster_title_2_handle',
        'description' => __('Please Title', 'kingster'),
        'type' => 'text'
    )));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'kingster_title_3_text_handle', array(
        'label' => __('Title', 'kingster'),
        'section' => 'kingster_section_3_section',
        'settings' => 'kingster_title_3_handle',
        'description' => __('Please Title', 'kingster'),
        'type' => 'text'
    )));



    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'kingster_content_1_text_handle', array(
        'label' => __('Content', 'kingster'),
        'section' => 'kingster_section_1_section',
        'settings' => 'kingster_content_1_handle',
        'description' => __('Please Enter Content', 'kingster'),
        'type' => 'textarea'
    )));

    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'kingster_content_2_text_handle', array(
        'label' => __('Content', 'kingster'),
        'section' => 'kingster_section_2_section',
        'settings' => 'kingster_content_2_handle',
        'description' => __('Please Enter Content', 'kingster'),
        'type' => 'textarea'
    )));

    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'kingster_content_3_text_handle', array(
        'label' => __('Content', 'kingster'),
        'section' => 'kingster_section_3_section',
        'settings' => 'kingster_content_3_handle',
        'description' => __('Please Enter Content', 'kingster'),
        'type' => 'textarea'
    )));

    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'kingster_image_1_text_handle', array(
        'label' => __('Icon', 'kingster'),
        'section' => 'kingster_section_1_section',
        'settings' => 'kingster_image_1_handle',
    )));

    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'kingster_image_2_text_handle', array(
        'label' => __('Icon', 'kingster'),
        'section' => 'kingster_section_2_section',
        'settings' => 'kingster_image_2_handle',
    )));

    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'kingster_image_3_text_handle', array(
        'label' => __('Icon', 'kingster'),
        'section' => 'kingster_section_3_section',
        'settings' => 'kingster_image_3_handle',
    )));

}