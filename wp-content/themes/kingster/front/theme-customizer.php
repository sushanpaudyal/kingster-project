<?php
   function kingster_customize_register($wp_customize){
       $wp_customize->get_section('title_tagline')->title = "General Settings";
       kingster_contact_customizer_section($wp_customize);
       kingster_social_customize_section($wp_customize);
       kingster_banner_customizer_section($wp_customize);

       $wp_customize->add_panel('basic', [
          'title' => __('Basic Details', 'kingster'),
          'description' => __('Contains Basic Information', 'kingster'),
          'priority' => 100
       ]);

       $wp_customize->add_panel('banner', [
           'title' => __('Banner Settings', 'kingster'),
           'description' => __('Contains banner Information', 'kingster'),
           'priority' => 100
       ]);

   }